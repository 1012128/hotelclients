﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LapTrinhHienDai_128_178_474.Models;
using LapTrinhHienDai_128_178_474.Helpers;
using System.Data.Entity.Validation;
using System.Net;
using System.Net.Http;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using WebMatrix.WebData;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using HtmlAgilityPack;

namespace LapTrinhHienDai_128_178_474.Controllers
{
    public class HotelController : Controller
    {
        //
        // GET: /Hotel/

        PortalEntities dbContext = new PortalEntities();

        //số kết quả mặc định trong một trang
        public const int PageSize = 8;


        //duyệt khách sạn theo thành phố
        public ActionResult HotelList(string City, string sortBy, int page)
        {
            var hotels = new PagedData<KhachSan>();
            switch (sortBy)
            {
                case "NAME":
                    hotels.Data = dbContext.KhachSan.Where(ks => ks.ThanhPho == City).OrderBy(ks => ks.TenKhachSan).Skip(PageSize * (page - 1)).Take(PageSize).ToList();
                    break;
                case "PRICE":
                    hotels.Data = dbContext.KhachSan.Where(ks => ks.ThanhPho == City).OrderBy(ks => ks.GiaTien).Skip(PageSize * (page - 1)).Take(PageSize).ToList();
                    break;
                case "CATEGORY":
                    hotels.Data = dbContext.KhachSan.Where(ks => ks.ThanhPho == City).OrderBy(ks => ks.HangKhachSan).Skip(PageSize * (page - 1)).Take(PageSize).ToList();
                    break;
                case "RATING":
                    hotels.Data = dbContext.KhachSan.Where(ks => ks.ThanhPho == City).OrderBy(ks => ks.DanhGia).Skip(PageSize * (page - 1)).Take(PageSize).ToList();
                    break;
                default:
                    hotels.Data = dbContext.KhachSan.Where(ks => ks.ThanhPho == City).OrderBy(ks => ks.TenKhachSan).Skip(PageSize * (page - 1)).Take(PageSize).ToList();
                    break;
            }
            hotels.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)dbContext.KhachSan.Where(ks => ks.ThanhPho == City).Count() / PageSize));
            hotels.CurrentPage = page;
            return PartialView(hotels);
        }


        //tìm kiếm khách sạn
        [AllowAnonymous]
        public ActionResult Search(string City,string HotelName,string sortBy)
        {
            var hotels = new PagedData<KhachSan>();
            if (String.IsNullOrEmpty(HotelName))
            {
                switch (sortBy)
                {
                    case "NAME":
                        hotels.Data = dbContext.KhachSan.Where(ks => ks.ThanhPho == City).OrderBy(ks => ks.TenKhachSan).Take(PageSize).ToList();
                        break;
                    case "PRICE":
                        hotels.Data = dbContext.KhachSan.Where(ks => ks.ThanhPho == City).OrderBy(ks => ks.GiaTien).Take(PageSize).ToList();
                        break;
                    case "CATEGORY":
                        hotels.Data = dbContext.KhachSan.Where(ks => ks.ThanhPho == City).OrderByDescending(ks => ks.HangKhachSan).Take(PageSize).ToList();
                        break;
                    case "RATING":
                        hotels.Data = dbContext.KhachSan.Where(ks => ks.ThanhPho == City).OrderByDescending(ks => ks.DanhGia).Take(PageSize).ToList();
                        break;
                    default:
                        hotels.Data = dbContext.KhachSan.Where(ks => ks.ThanhPho == City).OrderBy(ks => ks.TenKhachSan).Take(PageSize).ToList();
                        break;
                }
            }
            else
            {
                var khachSanCanTim = (from ks in dbContext.KhachSan
                                      where ks.TenKhachSan.ToLower().Contains(HotelName.ToLower())
                                      select ks).ToList();
                if (khachSanCanTim == null){
                    //return ViewResult("NotFound");
                }
                else{
                    hotels.Data = khachSanCanTim;
                }
            }
            hotels.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)dbContext.KhachSan.Where(ks => ks.ThanhPho == City).Count() / PageSize));
            hotels.CurrentPage = 1;
            ViewBag.CityCode = City;
            ViewBag.CityName = hotels.Data.FirstOrDefault().ThanhPho1.TenThanhPho;
            return View(hotels);
        }
  
        //trang chi tiêt khách sạn
        public ActionResult Detail(int Id)
        {
            KhachSan hotel = dbContext.KhachSan.Find(Id);
            ViewBag.CityName = hotel.ThanhPho1.TenThanhPho;

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:55113/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = client.GetAsync("api/Sheraton/LayDanhSachLoaiPhong").Result;
            if (response.IsSuccessStatusCode )
            {
                ViewBag.LoaiPhongs = response.Content.ReadAsAsync<List<LoaiPhong>>().Result;
            }

            //trích rút nội dung website
            //string webUrl = "http://www.buaxua.vn/Thong-tin-can-biet/Cac-dia-diem-tham-quan-du-lich-tai-Ha-Noi.html";
            //HtmlDocument htmlDoc = new HtmlDocument();
            //HtmlWeb webGet = new HtmlWeb();
            //htmlDoc = webGet.Load(webUrl);

            //var _content = htmlDoc.DocumentNode.SelectSingleNode("//body");
            //ViewBag.NoiDungRutTrich = _content.InnerHtml;
            ViewBag.NoiDungRutTrich = "HelloWorld";
            return View(hotel); 
        }

        //xem danh sách bình luận
        [ChildActionOnly]
        public ActionResult CommentList(List<BinhLuan> Comments)
        {
            return PartialView(Comments);
        }

        [HttpPost]
        [Authorize]
        public PartialViewResult AddComment(String HotelId, String CommentRating, String CommentContent)
        {
            //var hotel = dbContext.KhachSan.Find(int.Parse(HotelId));
            BinhLuan binhLuan = new BinhLuan();
            binhLuan.NoiDung = CommentContent;
            binhLuan.DiemDanhGia = int.Parse(CommentRating);
            binhLuan.NguoiBinhLuan = User.Identity.Name;
            binhLuan.ThoiGian = DateTime.Now.ToLocalTime().ToString();
            binhLuan.KhachSan = int.Parse(HotelId);
            //binhLuan.KhachSan1 = hotel;
            switch (CommentRating)
            {
                case "1":
                    binhLuan.TieuDe = "Rất không hài lòng";
                    break;
                case "2":
                    binhLuan.TieuDe = "Không hài lòng";
                    break;
                case "3":
                    binhLuan.TieuDe = "Tạm được";
                    break;
                case "4":
                    binhLuan.TieuDe = "Hài lòng";
                    break;
                case "5":
                    binhLuan.TieuDe = "Rất hài lòng";
                    break;
                default:
                    binhLuan.TieuDe = "Không rõ";
                    break;
            }
            dbContext.BinhLuan.Add(binhLuan);
            try
            {
                dbContext.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                //foreach (var err in e.EntityValidationErrors)
                //{
                //    foreach (var err1 in err.ValidationErrors)
                //    {
                //        Console.WriteLine(err1.ErrorMessage);
                //    }
                //}
            }
            
            return PartialView("SingleComment", binhLuan);
        }


        [Authorize]
        public ActionResult LichSuDatPhong()
        {
            var lichSuDatPhong = (from dp in dbContext.DonDatPhong where dp.MaKhachHang == User.Identity.Name.Trim() select dp).ToList();
            ViewBag.LichSuDatPhong = lichSuDatPhong;
            return View();
        }
    }
}

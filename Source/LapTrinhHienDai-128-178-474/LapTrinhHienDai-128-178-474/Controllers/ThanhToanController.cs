﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LapTrinhHienDai_128_178_474.Models;
using WebMatrix.WebData;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Data.Entity.Validation;
using System.Text;

namespace LapTrinhHienDai_128_178_474.Controllers
{
    public class ThanhToanController : Controller
    {

        PortalEntities db = new PortalEntities();
        //
        // GET: /ThanhToan/

        [HttpPost]
        [Authorize]
        public ActionResult Index(FormCollection form)
        {
            DonDatPhong donDatPhong = new DonDatPhong(){
                MaKhachHang = User.Identity.Name,
                ThoiGianDat = DateTime.Now.ToString(),
                NgayNhanPhong = form["NgayNhanPhong"].ToString(),
                NgayTraPhong = form["NgayTraPhong"].ToString(),
                MaKhachSan = int.Parse(form["MaKhachSan"].ToString()),
                TongTien = 0
            };

            int SoPhongNormal = int.Parse(form["normal"].ToString());
            int SoPhongVip = int.Parse(form["vip"].ToString());

            //đơn đặt phòng bình dân
            if (SoPhongNormal != 0){
                ChiTietDatPhong datPhongBinhDan = new ChiTietDatPhong(){
                    MaLoaiPhong = "normal",
                    TenLoaiPhong = "Phòng Bình Dân",
                    SoLuong = SoPhongNormal,
                    GiaTien = SoPhongNormal * int.Parse(form["GiaPhongnormal"].ToString())
                };
                donDatPhong.ChiTietDatPhong.Add(datPhongBinhDan);
            }

            //đơn đặt phòng vip
            if (SoPhongVip != 0)
            {
                ChiTietDatPhong datPhongVip = new ChiTietDatPhong(){
                    MaLoaiPhong = "vip",
                    TenLoaiPhong = "Phòng VIP",
                    SoLuong = SoPhongVip,
                    GiaTien = SoPhongVip * int.Parse(form["GiaPhongvip"].ToString())
                };
                donDatPhong.ChiTietDatPhong.Add(datPhongVip);
            }

            //cập nhật số tiền cho đơn đặt phòng
            foreach (ChiTietDatPhong ct in donDatPhong.ChiTietDatPhong.ToList())
            {
                donDatPhong.TongTien += ct.GiaTien;
            }

            ViewBag.KhachSanMuonDat = db.KhachSan.Find(int.Parse(form["MaKhachSan"].ToString()));
            
            ////lấy thông tin người dùng đang đăng nhập
            //if (WebSecurity.IsAuthenticated)
            //{
            //    //if (!WebSecurity.Initialized)
            //    //{
            //    //    WebSecurity.InitializeDatabaseConnection(
            //    //        "DefaultConnection", "UserProfile", "UserId", "UserName",
            //    //        autoCreateTables: false);
            //    //}

            //    UsersContext uc = new UsersContext();
            //    UserProfile currentUser = uc.UserProfiles.Where(x => x.UserId == WebSecurity.CurrentUserId).FirstOrDefault();

            //    ViewBag.NguoiDatPhong = currentUser.FullName;
            //    ViewBag.EmailDatPhong = currentUser.Email;
            //}

            //lưu vào Session
            Session["DonDatPhong"] = donDatPhong;
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult HoanTatThanhToan()
        {
            //thêm đơn đặt phòng
            DonDatPhong donDatPhong = Session["DonDatPhong"] as DonDatPhong;
            db.DonDatPhong.Add(donDatPhong);

            //thêm chi tiết đặt phòng cho đơn đặt phòng
            foreach (ChiTietDatPhong ct in donDatPhong.ChiTietDatPhong.ToList())
            {
                ct.MaDonDatPhong = donDatPhong.MaDon;
                db.ChiTietDatPhong.Add(ct);
            }

            try
            {
                //lưu cơ sở dữ liệu phía Portal
                db.SaveChanges();


                HttpClient client = new HttpClient();
                MediaTypeFormatter jsonFormatter = new JsonMediaTypeFormatter();
                client.BaseAddress = new Uri("http://localhost:55113/"); //địa chỉ host service                       
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                ////cập nhật phía api = lệnh post
                foreach (ChiTietDatPhong ct in donDatPhong.ChiTietDatPhong.ToList())
                {
                    var ctApi = new ModelDatPhong() { MaLoaiPhong = ct.MaLoaiPhong, SoLuong = ct.SoLuong.ToString(), NgayNhanPhong = donDatPhong.NgayNhanPhong, NgayTraPhong = donDatPhong.NgayTraPhong };
                    HttpContent _content = new ObjectContent<ModelDatPhong>(ctApi, jsonFormatter);
                    var result = client.PostAsync("api/Sheraton/DatPhong", _content).Result;
                }
            }
            catch (DbEntityValidationException dbValEx)
            {
                //var outputLines = new StringBuilder();
                //foreach (var eve in dbValEx.EntityValidationErrors)
                //{
                //    outputLines.AppendFormat("{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:"
                //      , DateTime.Now, eve.Entry.Entity.GetType().Name, eve.Entry.State);

                //    foreach (var ve in eve.ValidationErrors)
                //    {
                //        outputLines.AppendFormat("- Property: \"{0}\", Error: \"{1}\""
                //         , ve.PropertyName, ve.ErrorMessage);
                //    }
                //}

                //throw new DbEntityValidationException(string.Format("Validation errors\r\n{0}"
                // , outputLines.ToString()), dbValEx);
            }

            //sau khi lưu clear session
            //Session["DonDatPhong"] = null;
            return View();
        }

        public class ModelDatPhong
        {
            public String SoLuong { get; set; }
            public String NgayNhanPhong { get; set; }
            public String NgayTraPhong { get; set; }
            public String MaLoaiPhong { get; set; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LapTrinhHienDai_128_178_474.Models;

namespace LapTrinhHienDai_128_178_474.Controllers
{
    public class HomeController : Controller
    {
        PortalEntities dbContext = new PortalEntities();

        public ActionResult Index()
        {
            ViewBag.ThanhPhos = new SelectList(from p in dbContext.ThanhPho orderby p.TenThanhPho select p  , "MaThanhPho", "TenThanhPho");
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}

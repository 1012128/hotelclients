﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LapTrinhHienDai_128_178_474.Models
{
    public class LoaiPhong
    {
        public string MaLoaiPhong { get; set; }
        public string TenLoaiPhong { get; set; }
        public double GiaTien { get; set; }
        public int SoNguoiToiDa { get; set; }
    }

    public class Phong
    {
        public int MaPhong { get; set; }
        public string LoaiPhong { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace LapTrinhHienDai_128_178_474.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<ExternalUserInformation> ExternalUsers { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }

        //
        public string FullName { get; set; }
        public string Email { get; set; }
    }

    [Table("ExtraUserInformation")]
    public class ExternalUserInformation
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public bool? Verified { get; set; }
    }

    public class RegisterExternalLoginModel
    {
        [Required(ErrorMessage="Bạn chưa nhập id")]
        [Display(Name = "Nhập tài khoản của bạn")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }

        [Required(ErrorMessage="Bạn chưa nhập họ và tên")]
        [Display(Name = "Nhập họ và tên")]
        public string FullName { get; set; }

        [Required(ErrorMessage="Bạn chưa nhập địa chỉ email")]
        [Display(Name = "Nhập địa chỉ email")]
        [DataType(DataType.EmailAddress,ErrorMessage="Địa chỉ email không hợp lệ")]
        public string Email { get; set; }
    }



    public class LocalPasswordModel
    {
        [Required(ErrorMessage="Bạn chưa nhập mật khẩu cũ")]
        [DataType(DataType.Password)]
        [Display(Name = "Nhập mật khẩu hiện tại")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage="Bạn chưa nhập mật khẩu mới")]
        [StringLength(100, ErrorMessage = "Mật khẩu phải ít nhất {2} kí tự.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Nhập mật khẩu mới")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nhắc lại mật khẩu mới")]
        [Compare("NewPassword", ErrorMessage = "Mật khẩu nhắc lại không khớp.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required(ErrorMessage="Bạn chưa nhập tài khoản")]
        [MinLength(6,ErrorMessage="Tài khoản phải tối thiểu 6 kí tự")]
        [MaxLength(20,ErrorMessage="Tài khoản tối đa dài 20 kí tự")]
        [Display(Name = "Nhập tài khoản của bạn")]
        public string UserName { get; set; }

        [Required(ErrorMessage="Bạn chưa nhập mật khẩu")]
        [DataType(DataType.Password)]
        [Display(Name = "Nhập mật khẩu của bạn")]
        public string Password { get; set; }

        [Display(Name = "Ghi nhớ đăng nhập ?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required(ErrorMessage = "Bạn chưa nhập tài khoản")]
        [MinLength(6, ErrorMessage = "Tài khoản phải tối thiểu 6 kí tự")]
        [MaxLength(20, ErrorMessage = "Tài khoản tối đa dài 20 kí tự")]
        [Display(Name = "Nhập tài khoản của bạn")]
        public string UserName { get; set; }

        [Required(ErrorMessage="Bạn chưa nhập mật khẩu")]
        [StringLength(100, ErrorMessage = "Mật khẩu phải ít nhất {2} kí tự.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Nhập mật khẩu của bạn")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nhắc lại mật khẩu")]
        [Compare("Password", ErrorMessage = "Mật khẩu nhắc lại không khớp.")]
        public string ConfirmPassword { get; set; }


        [Required(ErrorMessage="Bạn chưa nhập họ tên đầy đủ")]
        [Display(Name="Nhập họ tên đầy đủ của bạn")]
        public string FullName { get; set; }
        
        [Required(ErrorMessage="Bạn chưa nhập email")]
        [DataType(DataType.EmailAddress,ErrorMessage="Địa chỉ email không hợp lệ")]
        [Display(Name="Nhập email của bạn")]
        public string Email { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
